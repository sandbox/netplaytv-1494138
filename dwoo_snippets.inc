<?php
/**
 * @file
 * Provides configuration form for the module
 * 
 * @copyright   Copyright (C) 2010/2011 NetPlayTV
 * @author      Marcelo Vani <marcelo.vani@netplaytv.com>
 * @date  20/02/2012
 *
 */
define ('DWOO_VALIDATE_PHP_TAGS',  1);
define ('DWOO_VALIDATE_DATA_DEFINED', 2);
define ('DWOO_VALIDATE_DATA_OVERRIDEN', 3);

function dwoo_snippet_list() {
  $header = array(array('data' => t('Description'), 
              'field' => 'description'), 
            array('data' => t('Variables'), 
              'field' => 'variables'),
            array('data' => t('Operations'), 
              'colspan' => 2)
          );
  
  //get snippets from DB
  $rows = dwoo_get_all_snippets($header);

  //convert object into array
  foreach ($rows as $row) {
    $ops = array();
    $ops[] = l(t('edit'), 'admin/settings/dwoo/snippets/edit/' . $row->id, array('query' => drupal_get_destination()));
    $ops[] = l(t('delete'), 'admin/settings/dwoo/snippets/delete/' . $row->id, array('query' => drupal_get_destination()));
    
    $snippets[$row->id]['description'] = check_plain($row->description);
    $snippets[$row->id]['variables'] = '<pre>' . check_plain($row->variables) . '</pre>';
    $snippets[$row->id]['operations'] = implode(' ', $ops);
  }

  if (empty($snippets)) {
    $snippets[] = array(array('data' => t('There are no snippets yet.'), 'colspan' => 4));
  }
  
  //show table
  return theme('table', $header, $snippets);
}

function dwoo_snippets_add() {
  return drupal_get_form('dwoo_snippets_form', 'add');
}

function dwoo_snippets_edit($id) {
  return drupal_get_form('dwoo_snippets_form', 'edit', $id);
}

function dwoo_snippets_delete($id) {
  return drupal_get_form('dwoo_snippets_delete_form', 'delete', $id);
}

/**
 * Form used to input and edit snippets
 */ 
function dwoo_snippets_form($form_state, $op, $id = NULL) {
  $values = new stdClass;
  
  $values->id = NULL;
  $values->description = NULL;
  $values->snippet = NULL;
  
  switch (strtolower($op)) {
    case 'edit':
      $snippets = dwoo_get_snippets($id);
      $values = $snippets[0];
    case 'add':
      $form['snippets'] = array(
        '#type' => 'fieldset',
        '#title' => 'snippets',
        '#collapsible' => FALSE,
        '#description' => check_plain(t('Snippets are processed by Dwoo. It must be written in PHP. The outcome of the snippet must be stored into the array $dwoo_data.')),
      );
        $form['snippets']['id'] = array(
           '#type' => 'hidden',
           '#title' => 'Snippet Id',
           '#default_value' => $values->id,
        );      
        $form['snippets']['description'] = array(
           '#type' => 'textfield',
           '#title' => 'Description',
           '#required' => TRUE,
           '#default_value' => $values->description,
           '#description' => t('You can give a description to your snippet.'),
        ); 
        
        $default_snippet = '$dwoo_data[\'name\'] = \'foo bar\';' . PHP_EOL;
        
        $form['snippets']['snippet'] = array(
           '#type' => 'textarea',
           '#title' => 'Snippet',
           '#required' => TRUE,
           '#default_value' => (!empty($values->snippet))?$values->snippet:$default_snippet,
           '#description' => t('You do not need to use enclose the code between %php. <br>
                       The snippet must populate values into the array $dwoo_data i.e. $dwoo_data[\'variable\'] = \'value\'. <br>
                       Do not use "$dwoo_data = array()" as this will result in the previus values being overriden', array('%php' => '<?php ?>')),
        );
        $form['snippets']['variables'] = array(
           '#type' => 'hidden',
           '#title' => 'Variables',
           '#default_value' => dwoo_get_snippet_variables($values->snippet),
           '#description' => t('Variables populated by the snippet. This field is auto-populated.'),
        );
        
        $form['snippets']['mode'] = array(
           '#type' => 'hidden',
           '#title' => 'mode',
           '#weight' => 20,
           '#default_value' => $op,
           '#description' => t('Used to pass the mode to the form submit. i.e. add or edit'),
        );    
       $form['submit'] = array(
         '#type' => 'submit',
         '#weight' => 21, 
         '#value' => t('Save'),
       );
       $form['reset'] = array(
         '#type' => 'button',
         '#weight' => 22, 
         '#value' => t('Cancel'),
       );
       break;  
  }

  return $form;
}

function dwoo_snippets_form_validate($form_id, &$form_state) {
  $op = $form_state['values']['op'];

  switch (strtolower($op)) {
    case 'save':
      $code = $form_state['values']['snippet'];
    
      //check for use of php opening and closing tags
      if (!dwoo_validate_code($code, DWOO_VALIDATE_PHP_TAGS)) {
        form_set_error('snippets', t('You must not use the PHP open and close tags %php', array('%php' => '<?php ?>')));
      }
    
      //check if variable $dwoo_data was populated in the snippet
      if (!dwoo_validate_code($code, DWOO_VALIDATE_DATA_DEFINED)) {
        form_set_error('snippets', t('The snippet must populate values into the array $dwoo_data i.e. $dwoo_data[\'foo\'] = \'bar\';'));
      }
    
      //check for overrides to the $dwoo_data variable
      if (!dwoo_validate_code($code, DWOO_VALIDATE_DATA_OVERRIDEN)) {
        form_set_error('snippets', t('Using "$dwoo_data = foo" will override previous data, use $dwoo_data[\'foo\'] = bar" instead'));
      }
      break;
    case 'cancel':
      //clear any error messages from validation
      drupal_get_messages();
      drupal_goto('admin/settings/dwoo/snippets');
      break;
  }
}
   
function dwoo_snippets_form_submit($form_id, &$form_state) {
  $edit = $form_state['values'];

  switch (strtolower($edit['op'])) { //op is Drupal default i.e. save
    case 'save':
      switch (strtolower($edit['mode'])) {//mode is the variable passed from the form. i.e. add, edit
        case 'add':
          $result = db_query("INSERT INTO {dwoo_snippets} (description, snippet, variables) VALUES ('%s','%s','%s')", $edit['description'], $edit['snippet'], dwoo_get_snippet_variables($edit['snippet']));
          if (db_affected_rows()) {
            drupal_set_message(t('The snippet has been added.'));
          } 
          else {
            drupal_set_message(t('Error adding snippet.'));
          }
          break;
        case 'edit':
          $result = db_query("UPDATE {dwoo_snippets} SET description = '%s', snippet = '%s', variables = '%s' WHERE id = %d", $edit['description'], $edit['snippet'], dwoo_get_snippet_variables($edit['snippet']), $edit['id']);
          if (db_affected_rows()) {
            drupal_set_message(t('The snippet has been updated.'));
          } 
          else {
            drupal_set_message(t('Error updating snippet.'));
          }
          break;
      }
      break;
    case 'cancel':
    default:
      drupal_set_message(t('Operation canceled.'));
  }
  $form_state['redirect'] = 'admin/settings/dwoo/snippets';
}

/*
 * Dwoo validate code
 * 
 */
function dwoo_validate_code($code, $dwoo_validate) {
  $result = TRUE;
  switch ($dwoo_validate) {
    case DWOO_VALIDATE_PHP_TAGS:
      $result = (preg_match('/(\<\?|\?\>)/si', $code, $matches) == 0);
      break;
    case DWOO_VALIDATE_DATA_DEFINED:
      $result = (preg_match('/(\$dwoo_data\s?\[.*\])/si', $code, $matches) > 0);
      break;
    case DWOO_VALIDATE_DATA_OVERRIDEN:
      $result = (preg_match('/(\$dwoo_data\s?[^\[]?=)/si', $code, $matches) == 0);
      break;
  }
  return $result;
}

/**
 * Form used to input and edit snippets
 */ 
function dwoo_snippets_delete_form($form_state, $op, $id = NULL) {
  $snippets = dwoo_get_snippets($id);
  $values = $snippets[0];
  
  $confirm_form = array();
  $confirm_form['id'] = array('#type' => 'hidden', '#value' => $id);
  
  $form = confirm_form($confirm_form,
            t('Are you sure you want to delete the snippet: %snippet?', array('%snippet' => $values->description)), 
              'admin/settings/dwoo',
            t('This action cannot be undone.'),
            t('Delete'),
            t('Cancel'));    
  
     return $form;
}

function dwoo_snippets_delete_form_submit($form_id, &$form_state) {
  $edit = $form_state['values'];
  
  $result = db_query("DELETE from {dwoo_snippets} WHERE id = %d", $edit['id']);
  if (db_affected_rows()) {
    drupal_set_message(t('The snippet has been deleted.'));
  } 
  else {
    drupal_set_message(t('Error deleting snippet.'));
  }
}

function dwoo_populate_snippets() {
  static $dwoo_snippets;
  static $dwoo_data;
  
  if (!isset($dwoo_snippets)) {
    $dwoo_snippets = TRUE;
    $rows = dwoo_get_all_snippets();

    foreach ($rows as $row) {
      $code = $row->snippet;
      $code_ok = TRUE;
      if (!dwoo_validate_code($code, DWOO_VALIDATE_PHP_TAGS)) {
        $code_ok = FALSE;
        watchdog('Dwoo eval error',
            t('Snippet %description. You must not use the PHP open and close tags.', 
            array('%description' => $row->description)), 
            array(), WATCHDOG_NOTICE);
      }
    
      //check if variable $dwoo_data was populated in the snippet
      if (!dwoo_validate_code($code, DWOO_VALIDATE_DATA_DEFINED)) {
        $code_ok = FALSE;
        watchdog('Dwoo eval error',
              t('Snippet %description. The snippet must populate values into the array $dwoo_data i.e. $dwoo_data[\'foo\'] = \'bar\';', 
              array('%description' => $row->description)), 
              array(), WATCHDOG_NOTICE);
      }
    
      //check for overrides to the $dwoo_data variable
      if (!dwoo_validate_code($code, DWOO_VALIDATE_DATA_OVERRIDEN)) {
        $code_ok = FALSE;
        watchdog('Dwoo eval error',
              t('Snippet %description. Using "$dwoo_data = foo" will override previous data, use $dwoo_data[\'foo\'] = bar" instead', 
              array('%description' => $row->description)), 
              array(), WATCHDOG_NOTICE);
      }
      
      if ($code_ok) {
            eval($code);
      }
    }

    /*
    ob_start();
    print eval($code);
    $output = ob_get_contents();
    ob_end_clean();
    return $output;
    */
  }
  return $dwoo_data;
}

/*
 * get_snippet_variables
 * Extract all variables used in the snippet
 */
function dwoo_get_snippet_variables($snippet) {
  preg_match_all('|\$dwoo_data(\s*)\[([^ ]*)\]|i', $snippet, $variables);
  if (isset($variables[2])) {
    $variables = preg_replace('/[^a-zA-Z0-9_]/', '', $variables[2]);
    return implode(', ', $variables);
  }
}

function dwoo_get_all_snippets($order = '') {
  return dwoo_get_snippets(NULL, $order);
}

function dwoo_get_snippets($id = NULL, $order = '') {
  $snippets = array();
  
  if (is_NULL($id)) {
    $query = "select * from {dwoo_snippets}";
    if (!empty($order)) { 
      $query .= tablesort_sql($order);
    }
    $result = db_query($query);
  } 
  else {
    $query = "select * from {dwoo_snippets} where id = %d";
    $result = db_query($query, $id);
  }
  
  while ($row = db_fetch_object($result)) {
    $snippets[] = $row;
  } 

  return $snippets;
}

