<?php
/**
 * @file
 * Provides configuration form for the module
 * 
 * @copyright   Copyright (C) 2010/2011 NetPlayTV
 * @author      Marcelo Vani <marcelo.vani@netplaytv.com>
 * @date  20/02/2012
 *
 */

/*
 * Implementation of hook_init()
 */
function dwoo_init() {
  module_load_include('inc', 'dwoo', 'dwoo_snippets');
}

/**
 * Implementation of hook_perm().
 */
function dwoo_perm() {
  return array('access administration pages');
}

/**
 * Implementation of hook_menu().
 */
function dwoo_menu() {
  $items['admin/settings/dwoo'] = array(
    'title' => 'Dwoo',
    'page callback' => 'dwoo_snippet_list',
    'access arguments' => array('access administration pages'),
    'type' => MENU_NORMAL_ITEM,
    'file' => 'dwoo_snippets.inc',
  );  
  $items['admin/settings/dwoo/snippets'] = array(
    'title' => 'Snippets',
    'page callback' => 'dwoo_snippet_list',
    'access arguments' => array('access administration pages'),
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => 0,
    'file' => 'dwoo_snippets.inc',
  );
  $items['admin/settings/dwoo/snippets/edit/%'] = array(
    'title' => 'Edit',
    'page callback' => 'dwoo_snippets_edit',
    'page arguments' => array(5),
    'access arguments' => array('access administration pages'),
    'type' => MENU_CALLBACK,
    'weight' => 4,
    'file' => 'dwoo_snippets.inc',
  );
  $items['admin/settings/dwoo/snippets/delete/%'] = array(
    'title' => 'Delete',
    'page callback' => 'dwoo_snippets_delete',
    'page arguments' => array(5),
    'access arguments' => array('access administration pages'),
    'type' => MENU_CALLBACK,
    'weight' => 4,
    'file' => 'dwoo_snippets.inc',
  );
  $items['admin/settings/dwoo/add'] = array(
    'title' => 'Add',
    'page callback' => 'dwoo_snippets_add',
    'page arguments' => array(3),
    'access arguments' => array('access administration pages'),
    'type' => MENU_LOCAL_TASK,
    'weight' => 5,
    'file' => 'dwoo_snippets.inc',
  );  
  $items['admin/settings/dwoo/config'] = array(
    'title' => 'Configuration',
    'description' => t('Dwoo Configuration'),
  'page callback' => 'drupal_get_form',
  'page arguments' => array('dwoo_configuration_form'),
    'access arguments' => array('access administration pages'),
    'type' => MENU_LOCAL_TASK,
  'weight' => 6,  
    'file' => 'dwoo.admin.inc',
  );  
  return $items;
}

/**
 * @file
 * Allows Dwoo to be used as input filter. Provides an interface for managing replacement scripts.
 */

/**
 * Implementation of hook_filter.
 * 
 * Adds the Dwoo filter to the input format options.
 */
function dwoo_filter($op, $delta = 0, $format = -1, $text = '' ) {
  switch ($op) {
    case 'list':
      return array(0 => t('Dwoo filter'));
    case 'description':
      return t('Allows you to use Dwoo replacement strings in a filter. i.e. {function args}');
    case 'settings':
      break;
    case 'no cache':
      return TRUE;
    case 'prepare':
      return $text;
    case 'process':
      return dwoo_process($text);
    default:
      return $text;
  } 
}

/*
 * Dwoo Process
 * Processes content using Dwoo
 */
function dwoo_process($text) {
  static $dwoo;
  static $dwoo_compiler;

  $dwoo_data = dwoo_populate_snippets();
  
  $output = $text;

  $dwoo_plugin_path = drupal_get_path('module', 'dwoo') . '/dwoo/Dwoo/Plugin.php';
  $dwoo_compiler_path = drupal_get_path('module', 'dwoo') . '/dwoo/Dwoo/Compiler.php';
  $dwoo_engine_path = drupal_get_path('module', 'dwoo') . '/dwoo/dwooAutoload.php';
  
  if (file_exists($dwoo_engine_path)) {
    include_once($dwoo_plugin_path);
    include_once($dwoo_engine_path);
    include_once($dwoo_compiler_path);

    try {
      //Create the controller, it is reusable and can render multiple templates
      if (!isset($dwoo)) {
        //custom compiled folder
        $custom_compiled_folder = variable_get('dwoo_custom_compiled_folder', drupal_get_path('module', 'dwoo') . '/dwoo/compiled');
        if (file_exists($custom_compiled_folder)) {
          $dwoo = new Dwoo($custom_compiled_folder);
        } 
        else {
          $dwoo = new Dwoo();
        }
        
        $dwoo_compiler = new Dwoo_Compiler();
        
        //add custom plugins folder
        $custom_plugins_folder = variable_get('dwoo_custom_plugins_folder', drupal_get_path('module', 'dwoo') . '/dwoo/Plugins');
        if (file_exists($custom_plugins_folder)) {
          $dwoo->getLoader()->addDirectory($custom_plugins_folder);
        }
      } 

      //update delimiters in case the settings have been changed
      $dwoo_compiler->setDelimiters(variable_get('dwoo_prefix_delimiter', '{'), variable_get('dwoo_sufix_delimiter', '}'));
      
      //crete the template object
      $tpl = new Dwoo_Template_String($text);  
  
      $output = $dwoo->get($tpl, $dwoo_data, $dwoo_compiler);
    } catch (Exception $e) {
      drupal_set_message(check_plain('Dwoo error: ' . $e->getMessage()), 'error');
      $output = $e->getMessage() . PHP_EOL . $output;
    }
  } 
  else {
    drupal_set_message('In order to use Dwoo you need to install Dwoo engine into modules/dwoo/dwoo', 'error');
  }
  
  return $output;    
}

/*
 * Implementation of hook_flush_caches
 */
function dwoo_flush_caches() {
  //empty compiled folders
  $dwoo_compiled_folder = drupal_get_path('module', 'dwoo') . '/dwoo/compiled';
  $dwoo_custom_compiled_folder = variable_get('dwoo_custom_compiled_folder', drupal_get_path('module', 'dwoo') . '/dwoo/compiled');
  
  foreach (array($dwoo_compiled_folder, $dwoo_custom_compiled_folder) as $dir) {
    foreach (file_scan_directory($dir, '.php') as $file) {
      file_delete($file->filename);
    }
  }
}

