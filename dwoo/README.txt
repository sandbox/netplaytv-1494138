Download Dwoo from the website http://dwoo.org/download/dwoo/downloads

Extract it into dwoo/dwoo folder

The folder structure should be

/modules/
/modules/dwoo/
/modules/dwoo/dwoo/
/modules/dwoo/dwoo/cache
/modules/dwoo/dwoo/compiled
/modules/dwoo/dwoo/Dwoo
/modules/dwoo/dwoo/plugins
/modules/dwoo/dwoo/dwooAutoload.php
/modules/dwoo/dwoo/Dwoo.compiled.php
/modules/dwoo/dwoo/Dwoo.php
...

