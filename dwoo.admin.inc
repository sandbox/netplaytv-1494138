<?php
/**
 * @file
 * Provides configuration form for the module
 * 
 * @copyright   Copyright (C) 2010/2011 NetPlayTV
 * @author      Marcelo Vani <marcelo.vani@netplaytv.com>
 * @date  20/02/2012
 *
 */

function dwoo_configuration_form() {
  $form['compiler'] = array(
    '#type' => 'fieldset',
    '#title' => t('<div class="title">Compiler Settings</div>'),
    '#description'   => t('Delimiters used by Dwoo compiler. Please note that when changing the delimiters sometimes you will have to edit and save content again.'),
  );      
    $form['compiler']['dwoo_prefix_delimiter'] = array(
        '#type'          => 'textfield',
        '#title'         => t('Prefix delimiter'),
        '#size'     => '5',    
        '#default_value' => variable_get('dwoo_prefix_delimiter', '{'),
      );
      $form['compiler']['dwoo_sufix_delimiter'] = array(
        '#type'          => 'textfield',
        '#title'         => t('Suffix delimiter'),
        '#size'     => '5',    
        '#default_value' => variable_get('dwoo_sufix_delimiter', '}'),
      );
      $form['compiler']['dwoo_custom_plugins_folder'] = array(
        '#type'          => 'textfield',
          '#description'   => t('Dwoo scans dwoo/plugins folder. You can specify a custom plugins folder in addition to the original. <a href="http://wiki.dwoo.org/index.php/WritingPlugins">Here</a> is some help on writting plugins'),
        '#title'         => t('Custom Plugins folder'),
        '#size'     => '50',    
        '#default_value' => variable_get('dwoo_custom_plugins_folder', drupal_get_path('module', 'dwoo') . '/dwoo/Plugins'),
      );
      $form['compiler']['dwoo_custom_compiled_folder'] = array(
        '#type'          => 'textfield',
          '#description'   => t('Specify the compiled folder to be used as storage. This folder must have writting permissions (chmod 777).'),
        '#title'         => t('Compiled folder'),
        '#size'     => '50',    
        '#default_value' => variable_get('dwoo_custom_compiled_folder', drupal_get_path('module', 'dwoo') . '/dwoo/compiled'),
      );
  return system_settings_form($form);
}
